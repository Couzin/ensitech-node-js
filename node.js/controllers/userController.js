const User = require('../models/Users');
const bcrypt = require('bcrypt');

const userController = {
  getAll: async (req, res) => {
    try {
      const users = await User.find();
      return {
        status: 200,
        data: users
      }
    } catch (error) {
      console.log(error);
      return {
        status: 500,
        message: error
      }
    }
  },
  getOne: async (req, res) => {
    try {
      const user = await User.findById(req.params.id);
      return {
        status: 200,
        data: user
      }
    } catch (error) {
      console.log(error);
      return {
        status: 500,
        message: error
      }
    }
  },
  register: async (req, res) => {
    try {
      const user = new User(req.body);
      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(user.password, salt);
      await user.save();
      return {
        status: 201,
        message: 'User created'
      }
    } catch (error) {
      console.log(error);
      return {
        status: 500,
        message: error
      }
    }
  },
  update: async (req, res) => {
    try {
      await User.findByIdAndUpdate(req.params.id, req.body);
      return {
        status: 200,
        message: 'User updated'
      }
    } catch (error) {
      console.log(error);
      return {
        status: 500,
        message: error
      }
    }
  },
  delete: async (req, res) => {
    try {
      await User.findByIdAndRemove(req.params.id);
      return {
        status: 200,
        message: 'User deleted'
      }
    } catch (error) {
      console.log(error);
      return {
        status: 500,
        message: error
      }
    }
  }
}

module.exports = userController;