const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');

router.get('/', async (req, res) => {
  const response = await userController.getAll(req, res);
  res.status(response.status).json(response.data);
});

router.get('/:id', async (req, res) => {
  const response = await userController.getOne(req, res);
  res.status(response.status).json(response.data);
});

router.post('/', async (req, res) => {
    const response = await userController.register(req, res);
    res.status(response.status).json(response.message);
});

router.patch('/:id', async (req, res) => {
    const response = await userController.update(req, res);
    res.status(response.status).json(response.message);
});

router.delete('/:id', async (req, res) => {
    const response = await userController.delete(req, res);
    res.status(response.status).json(response.message);
});

module.exports = router;
