const bcrypt = require('bcrypt');

const userController = {
  register: (req, res) => {
    try {
      const user = req.body;
      user.password = bcrypt.hash(user.password, 10);
      req.session.user = user;
      res.redirect('/user/profile');
    } catch (E) {
      console.log(E);
      res.status(500).send(E.message);
      res.redirect('/register');
    }
  },
  login: (req, res) => {
    try {
      const user = req.body;

      if (user.username === "" || user.password === "") {
        res.status(400).send('Username or password is empty');
      }

      if (bcrypt.compare(user.password, req.session.user.password)) {
        res.status(200).render('profile', { username: req.session.username });
      } else {
        res.redirect('/');
      }
    } catch (E) {
      console.log(E);
      res.status(500).redirect('/');
    }
  }
};

module.exports = userController;