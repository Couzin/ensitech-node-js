const express = require('express');
const userController = require('../controllers/user');

const mainRouter = express.Router();

mainRouter.get('/', (req, res) => {
  res.render('login');
});

mainRouter.post('/login', (req, res) => {
  userController.login(req, res);
});

mainRouter.get('/register', (req, res) => {
  res.render('register');
});

mainRouter.post('/register', (req, res) => {
  userController.register(req, res);
});

mainRouter.get('/user/profile', (req, res) => {
  res.render('profile', { user: req.session.user });
});

mainRouter.get('/logout', (req, res) => {
  res.redirect('/');
});

module.exports = mainRouter;