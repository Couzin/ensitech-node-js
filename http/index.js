const http = require('node:http')

http.createServer((req, res) => {
  const statusCode = 200;
  res.writeHead(statusCode, {
    "Content-Type": "text/plain",
  });
  res.end("Hello HTTP");
}).listen(3000);