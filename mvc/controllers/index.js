const mainController = {
  index: (req, res) => {
    let username = null;
    if (req.query.username) {
      username = req.query.username;
    }
    res.render('home', {username: username});
  },
}

module.exports = mainController;