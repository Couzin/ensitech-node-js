const userController = {
  createUsername: (req, res) => {
    try {
      if (!req.body.username || req.body.username === "") {
        res.status(400).send("Please provide a username");
        res.redirect('/login');
      }

      let username = req.body.username;

      if (!req.session.user) {
        req.session.user = {}
      }

      req.session.user.name = username;
      res.redirect(`/user/profile`);
    } catch (E) {
      console.log(E);
      res.status(500).send("Internal Server Error");
    }
  },
  logout: (req, res) => {
    req.session.destroy();
    res.redirect('/login');
  }
};

module.exports = userController;