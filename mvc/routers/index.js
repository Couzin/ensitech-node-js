const express = require('express');
const controller = require('../controllers/');
const userRouter = require('./user');

const mainRouter = express.Router();

mainRouter.get('/', (req, res) => {
  controller.index(req, res);
});

mainRouter.use(userRouter)
module.exports = mainRouter;