const express = require('express');
const controller = require('../controllers/user');
const {isAuth} = require('../utils/middlewares/auth');
const userRouter = express.Router();

userRouter.get('/login', (req, res) => {
  res.render('user-form');
});

userRouter.post('/user', (req, res) => {
  controller.createUsername(req, res);
});

userRouter.get('/user/logout', (req, res) => {
  controller.logout(req, res);
});

userRouter.get('/user/profile', isAuth, (req, res) => {
  res.render('profile', { username: req.session.user.name });
});

module.exports = userRouter;