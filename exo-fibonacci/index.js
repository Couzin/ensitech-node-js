const express = require('express');
const router = require('./routers/');
const app = express();

app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');

app.use(router);

app.listen(3000, () => {
  console.log('Server is listening on port 3000');
});