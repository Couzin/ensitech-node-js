const getFibo = (results, n) => {
    if (n <= 1) {
        return n;
      } else {
        return getFibo(n - 1) + getFibo(n - 2);
      }
};

module.exports = getFibo;
