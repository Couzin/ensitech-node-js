const express = require('express');

const mainRouter = express.Router();

mainRouter.get('/', (req, res) => {
  res.render('index');
});

mainRouter.get('/fibonacci/:number', (req, res) => {
  const number = req.params.number;
  const result = controller.getResult(number);
  res.render('result', result);
});

module.exports = mainRouter;